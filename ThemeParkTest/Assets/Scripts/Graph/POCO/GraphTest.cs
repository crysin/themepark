﻿using System;
using NUnit;
using NUnit.Framework;
using Assets.Scripts.Graphing.POCO;
using UnityEngine;

namespace ThemeParkTest
{
    [TestFixture]
    public class GraphTest
    {
        [Test]
        public void CreateVertex_ReturnsAVertex()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            Assert.IsNotNull(vertex);
        }

        [Test]
        public void CreateVertex_ReturnsANewVertex()
        {
            Graph graph = new Graph();
            Assert.IsEmpty(graph.Vertices);
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            Assert.IsNotNull(vertex);
        }

        [Test]
        public void CreateVertex_ReturnsAnExistingVertexIfOneIsFoundInThatPosition()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            IVertex vertex2 = graph.CreateVertex(Vector2.zero);
            Assert.AreSame(vertex, vertex2);
        }

        [Test]
        public void GetVertexAtPosition_ReturnsNullIfNotVertexIsFoundAtPosition()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.GetVertexAtPosition(Vector2.zero);
            Assert.IsNull(vertex);
        }

        [Test]
        public void GetVertexAtPosition_ReturnsVertexIfOneIsFoundAtPosition()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            IVertex vertex2 = graph.GetVertexAtPosition(Vector2.zero);
            Assert.AreSame(vertex, vertex2);
        }

        [Test]
        public void AddToVertices_AddsVertexToVertices_When_CreateVertex_IsCalled()
        {
            Graph graph = new Graph();
            Assert.IsEmpty(graph.Vertices);
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            Assert.Contains(vertex, graph.Vertices);
        }

        [Test]
        public void RemoveVertex_RemovesFromVertices_WhenGivenAValidPosition()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            Assert.Contains(vertex, graph.Vertices);
            graph.RemoveVertex(Vector2.zero);
            Assert.IsEmpty(graph.Vertices);
        }

        [Test]
        public void RemoveVertex_RemovesFromVertices_WhenGivenAValidVertex()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            Assert.Contains(vertex, graph.Vertices);
            graph.RemoveVertex(vertex);
            Assert.IsEmpty(graph.Vertices);
        }

        [Test]
        public void RemoveVertex_RemovesConnectionFromNeighbor()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            IVertex neighbor = graph.CreateVertex(new Vector2(1, 0));
            Assert.Contains(vertex, graph.Vertices);
            Assert.True(neighbor.Connections.Count == 1);
            graph.RemoveVertex(vertex);
            Assert.True(neighbor.Connections.Count == 0);
        }

        [Test]
        public void RemoveVertex_DoesNotDoAnythingWhenPassedNullAndReturnsFalse()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            bool removedVert = graph.RemoveVertex(null);
            Assert.True(graph.Vertices.Contains(vertex));
            Assert.False(removedVert);
        }

        [Test]
        public void RemoveVertex_DoesNotDoAnythingWhenPassedInvalidPositionAndReturnsFalse()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(Vector2.zero);
            bool removedVert = graph.RemoveVertex(new Vector2(0, 1));
            Assert.True(graph.Vertices.Contains(vertex));
            Assert.False(removedVert);
        }

        [Test]
        public void GetNearestVertexToPosition_ReturnsNearestVertex()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(new Vector2(5, 5));
            IVertex vertex2 = graph.CreateVertex(new Vector2(6, 6));
            IVertex vertex3 = graph.CreateVertex(new Vector2(20, 20));
            IVertex closest = graph.GetNearestVertexToPosition(new Vector2(4, 3));
            Assert.AreSame(closest, vertex);
        }

        [Test]
        public void GetNearestVertexToPosition_ReturnsNullWhenNoVerticesAreInGraph()
        {
            Graph graph = new Graph();
            Assert.IsNull(graph.GetNearestVertexToPosition(new Vector3(0, 0)));
        }
    }
}
