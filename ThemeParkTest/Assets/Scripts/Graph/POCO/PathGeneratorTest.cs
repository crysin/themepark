﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Graphing.POCO;
using UnityEngine;

namespace ThemeParkTest
{
    [TestFixture]
    class PathGeneratorTest
    {
        [Test]
        public void CalculatorTest()
        {
            List<IVertex> createdVertices = new List<IVertex>();
            Graph graph = new Graph();
            createdVertices.Add(graph.CreateVertex(new Vector2(0, 1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(0, 2)));
            createdVertices.Add(graph.CreateVertex(new Vector2(1, 2)));
            createdVertices.Add(graph.CreateVertex(new Vector2(1, 3)));
            createdVertices.Add(graph.CreateVertex(new Vector2(1, 4)));
            createdVertices.Add(graph.CreateVertex(new Vector2(0, 4)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-1, 4)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-2, 4)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-2, 3)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-2, 2)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-2, 1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-3, 1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-4, 1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(2, 4)));
            createdVertices.Add(graph.CreateVertex(new Vector2(3, 4)));
            createdVertices.Add(graph.CreateVertex(new Vector2(4, 4)));
            createdVertices.Add(graph.CreateVertex(new Vector2(4, 3)));
            createdVertices.Add(graph.CreateVertex(new Vector2(4, 2)));
            createdVertices.Add(graph.CreateVertex(new Vector2(4, 1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(4, 0)));
            createdVertices.Add(graph.CreateVertex(new Vector2(4, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(3, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(2, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(1, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(0, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-1, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-2, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-3, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-4, -1)));
            createdVertices.Add(graph.CreateVertex(new Vector2(-4, 0)));

            PathGenerator pathGenerator = new PathGenerator();
            pathGenerator.GetPath(graph, createdVertices[0], createdVertices[4].Position, () => 
            {
                Assert.True(pathGenerator.Path.Count == 5);
                pathGenerator.GetPath(graph, createdVertices[0], createdVertices[6].Position, () => 
                {
                    Assert.True(pathGenerator.Path.Count == 7);
                });
                while (pathGenerator.Finished == false)
                {
                    pathGenerator.Update();
                }
            });
            while(pathGenerator.Finished == false)
            {
                pathGenerator.Update();
            }
        }

        [Test]
        public void PerformanceTest()
        {
            List<IVertex> createdVertices = new List<IVertex>();
            Graph graph = new Graph();
            for (int i = 0; i < 100; i++)
            {
                for(int j = 0; j < 100; j++)
                {
                    createdVertices.Add(graph.CreateVertex(new Vector2(i, j)));
                }
            }
            PathGenerator pathGenerator = new PathGenerator();
            pathGenerator.GetPath(graph, createdVertices[0], createdVertices[createdVertices.Count - 1].Position, () => 
            {
                Assert.True(pathGenerator.Finished);
            });
        }
    }
}
