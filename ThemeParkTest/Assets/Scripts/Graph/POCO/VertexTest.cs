﻿using System;
using NUnit;
using NUnit.Framework;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Graphing.POCO;
using Moq;

namespace ThemeParkTest
{
    [TestFixture]
    public class VertexTest
    {
        [Test]
        public void FillOutImmediateNeighborPositions_WhenCreateVertexIsCalled()
        {
            Graph graph = new Graph();
            IVertex vertex = graph.CreateVertex(new Vector2(1, 1));
            Assert.AreEqual(vertex.ImmediateNeighborPositions[0], new Vector2(1, 2));
            Assert.AreEqual(vertex.ImmediateNeighborPositions[1], new Vector2(2, 1));
            Assert.AreEqual(vertex.ImmediateNeighborPositions[2], new Vector2(1, 0));
            Assert.AreEqual(vertex.ImmediateNeighborPositions[3], new Vector2(0, 1));
        }

        [Test]
        public void AddConnection_WhenSecondVertexIsCreatedInANeighboringPosition()
        {
            Graph graph = new Graph();
            IVertex firstvertex = graph.CreateVertex(new Vector2(0, 0));
            Assert.IsEmpty(firstvertex.Connections);
            IVertex secondvertex = graph.CreateVertex(new Vector2(1, 0));
            Assert.True(firstvertex.Connections[0].Target == secondvertex);
            Assert.True(secondvertex.Connections[0].Target == firstvertex);
        }

        [Test]
        public void AddConnection_DoesNotDiagonalNeighbors()
        {
            Graph graph = new Graph();
            IVertex firstvertex = graph.CreateVertex(new Vector2(0, 0));
            Assert.IsEmpty(firstvertex.Connections);
            IVertex secondvertex = graph.CreateVertex(new Vector2(1, 1));
            Assert.IsEmpty(firstvertex.Connections);
            Assert.IsEmpty(secondvertex.Connections);
        }

        [Test]
        public void Remove_RemovesConnections_FromVertexThatIsBeingRemove_And_FromAllNeighborVertices()
        {
            Graph graph = new Graph();
            IVertex firstvertex = graph.CreateVertex(new Vector2(0, 0));
            IVertex secondvertex = graph.CreateVertex(new Vector2(1,0));
            IVertex thirdvertex = graph.CreateVertex(new Vector2(-1, 0));
            IEdge firstVertToSecondVert = firstvertex.Connections[0];
            IEdge firstVertToThirdVert = firstvertex.Connections[1];
            IEdge SecondVertToFirstVert = secondvertex.Connections[0];
            IEdge ThirdVertToFirstVert = thirdvertex.Connections[0];
           
            Assert.True(secondvertex.Connections.Count == 1);
            Assert.True(thirdvertex.Connections.Count == 1);

            graph.RemoveVertex(secondvertex);
            Assert.IsEmpty(secondvertex.Connections);

            graph.RemoveVertex(thirdvertex.Position);
            Assert.IsEmpty(thirdvertex.Connections);
        }

        [Test]
        public void Remove_WhenMiddleVertexIsRemove_NeighborVerticesDontConnectToEachOtherIfNotImmediateNeighbors()
        {
            Graph graph = new Graph();
            IVertex firstvertex = graph.CreateVertex(new Vector2(0, 0));
            IVertex secondvertex = graph.CreateVertex(new Vector2(1, 0));
            IVertex thirdvertex = graph.CreateVertex(new Vector2(-1, 0));
            IEdge firstVertToSecondVert = firstvertex.Connections[0];
            IEdge firstVertToThirdVert = firstvertex.Connections[1];
            IEdge SecondVertToFirstVert = secondvertex.Connections[0];
            IEdge ThirdVertToFirstVert = thirdvertex.Connections[0];

            Assert.True(secondvertex.Connections.Count == 1);
            Assert.True(thirdvertex.Connections.Count == 1);
            graph.RemoveVertex(firstvertex);
            Assert.IsEmpty(secondvertex.Connections);
            Assert.IsEmpty(thirdvertex.Connections);
        }

        [Test]
        public void SetGameObject_ReturnsTrueWhenItSetsAnObject()
        {
            Graph graph = new Graph();
            IVertex firstvertex = graph.CreateVertex(new Vector2(0, 0));
            bool setGameObject = firstvertex.SetGameObject(new object());
            Assert.IsTrue(setGameObject);
        }

        [Test]
        public void SetGameObject_ReturnsFalseWhenItTriesToSetAnObjectAfterAlreadyHavingSetOneBefore()
        {
            Graph graph = new Graph();
            IVertex firstvertex = graph.CreateVertex(new Vector2(0, 0));
            bool setGameObject = firstvertex.SetGameObject(new object());
            setGameObject = firstvertex.SetGameObject(new object());
            Assert.IsFalse(setGameObject);
        }

        [Test]
        public void SetDistanceFromStart_SetsDistance()
        {
            Graph graph = new Graph();
            IVertex firstvertex = graph.CreateVertex(new Vector2(0, 0));
            firstvertex.SetDistanceFromStart(this, 10f);
            Assert.AreEqual(firstvertex.DistanceFromStart[this], 10f, .0001f);
        }
    }
}
