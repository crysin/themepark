﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.AI.Unity;
using Assets.Scripts.Graphing.POCO;

public class TestbedLevelController : MonoBehaviour {

    public ArtificialIntelligenceManagerGameObject manager = null;
    public void Start()
    {
        Init();
    }

    private void Init()
    {
        manager.Init();
        GeneratePath();
        StartCoroutine(GenerateAgents());
    }

    private IEnumerator GenerateAgents()
    {
        for (int i = 0; i < 10; i++)
        {
            int random = Random.Range(0, manager.AttractionsController.Attractions.Count - 1);
            Vector3 goalPosition = manager.AttractionsController.Attractions[random].GoalPosition;
            manager.CreateAgent(goalPosition, new Vector3(Random.Range(0, 20), 0, Random.Range(0, 20)));
            yield return null;
        }
    }

    private void GeneratePath()
    {
        for (int i = 0; i < 20; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                int mod = i < 10 && j > 10 || i > 10 && j < 10 ? 1 : 2;
                if(i == 4 * mod || i == 5 * mod)
                {
                    if(j == 4 * mod || j == 5 * mod || j == 7 * mod || j == 8 * mod || j == 1 * mod || j == 2 * mod)
                    {
                        manager.AttractionsController.AddAttraction(new Vector3(i, 0, j));
                        continue;
                    }
                }
                if( i == 1 * mod || i == 8 * mod)
                {
                    if (j == 5 * mod || j == 6 * mod || j == 8 * mod || j == 9 * mod || j == 2 * mod || j == 3 * mod)
                    {
                        manager.AttractionsController.AddAttraction(new Vector3(i, 0, j));
                        continue;
                    }
                }
                manager.GraphController.CreateVertex(new Vector3(i,0,j));
            }
        }
    }
}
