﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Graphing.POCO
{
    public interface IEdge
    {
        IVertex Target { get; }
        float Distance { get; }
    }
}
