﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Graphing.POCO
{
    public class PathGenerator
    {
        private Dictionary<Guid, IVertex> predecessors = null;
        private List<IVertex> path = null;
        public ReadOnlyCollection<IVertex> Path { get { return path.AsReadOnly(); } }
        List<IVertex> unsettledVertices = null;
        private IVertex goal = null;
        private IEnumerator coroutine = null;
        private Action onFinish = null;
        public bool Finished { get; private set; }

        IVertex nextFoundNode = null;
        List<IEdge> connections = new List<IEdge>(); //node.Connections.Where(c => queue.Contains(c.Target));
        int nodeConnections = 0;
        int queueCount = 0;
        float closestDistance = float.PositiveInfinity;

        public void GetPath(Graph graph, IVertex startingVertex, Vector2 target, Action OnFinish)
        {
            Finished = false;
            predecessors = new Dictionary<Guid, IVertex>();
            goal = graph.GetNearestVertexToPosition(target);
            InitialiseGraph(graph, startingVertex);
            ProcessGraph(graph);
            onFinish = OnFinish;
        }

        private void GeneratePath()
        {
            path = new List<IVertex>();
            // check if a path exists
            if (!predecessors.ContainsKey(goal.ID))
            {
                coroutine = null;
                Finished = true;
                if (onFinish != null)
                {
                    onFinish();
                }
                return;
            }
            path.Add(goal);
            IVertex nextGoal = goal;
            while (predecessors.TryGetValue(nextGoal.ID, out nextGoal))
            {
                path.Add(nextGoal);
            }
            // Put it into the correct order
            path.Reverse();
            Finished = true;
            if (onFinish != null)
            {
                coroutine = null;
                onFinish();
            }
        }

        private void InitialiseGraph(Graph graph, IVertex startingVertex)
        {
            for (int i = 0; i < graph.Vertices.Count; i++)
            {
                IVertex vertex = graph.Vertices[i];
                vertex.SetDistanceFromStart(this, float.PositiveInfinity);
            }
            startingVertex.SetDistanceFromStart(this, 0);
        }

        private void ProcessGraph(Graph graph)
        {
            unsettledVertices = new List<IVertex>(graph.Vertices);
            int verticeCount = unsettledVertices.Count;
            for (int i = 0; i < verticeCount; i++)
            {
                IVertex vertex = unsettledVertices[i];
                if (vertex.DistanceFromStart[this] == 0)
                {
                    StartCoroutine(ProcessVertex, vertex, unsettledVertices, goal);
                    unsettledVertices.Remove(vertex);
                    break;
                }
            }
        }

        private IEnumerator ProcessVertex(IVertex node, List<IVertex> queue, IVertex goal)
        {
            nextFoundNode = null;
            connections = new List<IEdge>(); //node.Connections.Where(c => queue.Contains(c.Target));
            nodeConnections = node.Connections.Count;
            queueCount = queue.Count;

            closestDistance = float.PositiveInfinity;
            for (int i = 0; i < nodeConnections; i++)
            {
                IEdge currentConnection = node.Connections[i];
                for (int j = 0; j < queueCount; j++)
                {
                    if (queue[j] == currentConnection.Target)
                    {
                        float distance = node.DistanceFromStart[this] + currentConnection.Distance;
                        if (distance < currentConnection.Target.DistanceFromStart[this])
                        {
                            float dist = Vector2.Distance(currentConnection.Target.Position, goal.Position);
                            if (predecessors.ContainsKey(currentConnection.Target.ID))
                                continue;

                            predecessors.Add(currentConnection.Target.ID, node);
                            currentConnection.Target.SetDistanceFromStart(this, distance);
                            if (dist < closestDistance)
                            {
                                nextFoundNode = currentConnection.Target;
                                closestDistance = dist;
                            }
                        }
                        break;
                    }
                }
                yield return null;
            }
            ContinueProcessingGraph(nextFoundNode);
        }


        private void ContinueProcessingGraph(IVertex nextFoundNode)
        {
            if (nextFoundNode != null)
            {
                StartCoroutine(ProcessVertex, nextFoundNode, unsettledVertices, goal);
                unsettledVertices.Remove(nextFoundNode);
            }
            else
            {
                GeneratePath();
            }
        }

        private void StartCoroutine(Func<IVertex, List<IVertex>, IVertex, IEnumerator> func, IVertex node, List<IVertex> queue, IVertex goal)
        {
            coroutine = func(node, queue, goal);
        }

        // Updating just means stepping through all the coroutines
        public void Update()
        {
            if (coroutine != null)
            {
                coroutine.MoveNext();
            }
        }

        public void OnDestroy(Graph graph)
        {
            for(int i = 0; i < graph.Vertices.Count; i++)
            {
                IVertex vertex = graph.Vertices[i];
                vertex.RemovePathFinderDistance(this);
            }
        }
    }
}
