﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Collections.ObjectModel;
using Assets.Scripts.Utility.Dictionary;
namespace Assets.Scripts.Graphing.POCO
{
    public partial class Graph
    {
        private List<IVertex> vertices = new List<IVertex>();
        public ReadOnlyCollection<IVertex> Vertices { get { return vertices.AsReadOnly(); } }

        //Tested
        public IVertex CreateVertex(Vector2 position)
        {
            Vertex vertex = (Vertex)GetVertexAtPosition(position);
            if(vertex != null)
            {
                return vertex;
            }
            vertex = new Vertex(position, this);
            AddToVertices(vertex);
            return vertex;
        }
        //Tested
        public IVertex GetVertexAtPosition(Vector2 position)
        {
            IVertex vertex = new FindVertex(position, vertices).Find();
            return vertex;
        }

        public IVertex GetNearestVertexToPosition(Vector2 position)
        {
            IVertex closest = null; // vertices.OrderBy(vertex => Vector2.Distance(vertex.Position, position)).FirstOrDefault();
            float closestDistance = float.PositiveInfinity;
            int vertCount = vertices.Count;
            for(int i = 0; i < vertCount; i++)
            {
                IVertex vertex = vertices[i];
                float distance = Vector2.Distance(vertex.Position, position);
                if(distance < closestDistance)
                {
                    closest = vertex;
                    closestDistance = distance;
                }
            }
            return closest;
        }

        //Tested
        private void AddToVertices(Vertex vertex)
        {
            vertices.Add(vertex);
            vertices.OrderBy((n) => n.Position.x);
        }
        
        public bool RemoveVertex(Vector2 position)
        {
            IVertex vertex = new FindVertex(position, vertices).Find();
            return RemoveVertex(vertex);
        }

        public bool RemoveVertex(IVertex vertex)
        {
            if (vertex == null)
            {
                return false;
            }
            vertices.Remove(vertex);
            vertex.Remove();
            return true;
        }
    }
}

