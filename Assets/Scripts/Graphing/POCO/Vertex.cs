﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Assets.Scripts.Graphing.POCO
{
    public partial class Graph
    {
        private class Vertex : IVertex
        {
            public ReadOnlyCollection<IEdge> Connections { get { return data.Connections.AsReadOnly(); } }
            public Guid ID { get { return data.ID; } }
            public Vector2 Position { get { return data.Position; } }
            public Vector2[] ImmediateNeighborPositions { get { return data.ImmediateNeighborPositions; } }
            public object GameObject { get { return data.GameObject; } }
            public Dictionary<object, float> DistanceFromStart { get { return data.DistanceFromStart; } }

            public event Action<IVertex> RemoveFromNeighbors = null;
            public Action DataUpdate { get; set; }

            private VertexData data;
            private Graph graph = null;

            public Vertex(Vector2 position, Graph graph)
            {
                this.graph = graph;
                Vector2[] immediateNeighborPositions = FillOutImmediateNeighborPositions(position);
                data = new VertexData(Guid.NewGuid(), position, immediateNeighborPositions);
                GetEdges(immediateNeighborPositions);
            }

            private  void GetEdges(Vector2[] possibleNeighbors)
            {
                for (int i = 0; i < possibleNeighbors.Length; i++)
                {
                    Vertex neighbor = (new FindVertex(possibleNeighbors[i], graph.vertices).Find() as Vertex);
                    if (neighbor != null)
                    {
                        for (int j = 0; j < neighbor.Connections.Count; j++)
                        {
                            Edge edge = (Edge)neighbor.Connections[j];
                            if (edge.Target == this)
                            {
                                return;
                            }
                        }
                        AddPathConnection(this, neighbor);
                        AddPathConnection(neighbor, this);
                    }
                }
            }

            private void AddPathConnection(Vertex me, Vertex neighbor)
            {
                me.RemoveFromNeighbors += Remove;
                me.data.Connections.Add(new Edge(neighbor, 1));
                if (DataUpdate != null)
                {
                    DataUpdate();
                }
                if (neighbor.DataUpdate != null)
                {
                    neighbor.DataUpdate();
                }
            }

            private void Remove(IVertex vertex)
            {
                IEdge edge = data.Connections.Find((e) => e.Target == vertex);
                data.Connections.Remove(edge);
            }

            public void Remove()
            {
                if (Connections.Count > 0)
                {
                    RemoveFromNeighbors(this);
                }
                data.Connections.Clear();
            }

            public bool SetGameObject(object gameObject)
            {
                if (data.GameObject == null)
                {
                    data.GameObject = gameObject;
                    return true;
                }
                return false;
            }

            private Vector2[] FillOutImmediateNeighborPositions(Vector2 position)
            {
                Vector2[] possibleImmediateNeighborPositions = new Vector2[4];
                possibleImmediateNeighborPositions[0] = new Vector2(position.x, position.y + 1);
                possibleImmediateNeighborPositions[1] = new Vector2(position.x + 1, position.y);
                possibleImmediateNeighborPositions[2] = new Vector2(position.x, position.y - 1);
                possibleImmediateNeighborPositions[3] = new Vector2(position.x - 1, position.y);
                return possibleImmediateNeighborPositions;
            }

            public void SetDistanceFromStart(object pathFinder, float distance)
            {
                data.DistanceFromStart[pathFinder] = distance;
            }

            public void RemovePathFinderDistance(object pathFinder)
            {
                data.DistanceFromStart.Remove(pathFinder);
            }
        }
    }
}
