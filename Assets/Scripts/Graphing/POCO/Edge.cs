﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Graphing.POCO
{
    public class Edge : IEdge
    {
        private IVertex target; // Vertex one

        public IVertex Target { get { return target; } }

        private float distance; // DIstance or similar 
        
        public float Distance { get { return distance; } }  

        public Edge(IVertex target, float distance)
        {
            this.target = target;
            this.distance = distance;
        }
    }
}
