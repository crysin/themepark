﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Assets.Scripts.Graphing.POCO
{
    public interface IVertex
    {
        ReadOnlyCollection<IEdge> Connections { get; }
        System.Guid ID { get; }
        Vector2 Position { get; }
        Vector2[] ImmediateNeighborPositions { get; }
        System.Action DataUpdate { get; set; }
        Dictionary<object, float> DistanceFromStart { get; }
        void Remove();
        bool SetGameObject(object gameObject);
        object GameObject { get; }
        void SetDistanceFromStart(object pathFinder, float distance);
        void RemovePathFinderDistance(object pathFinder);
    }
}
