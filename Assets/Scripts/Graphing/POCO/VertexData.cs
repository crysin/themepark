﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Graphing.POCO
{
    partial class Graph
    {
        protected class VertexData
        {
            public Guid ID { get; set; }
            public Vector2 Position { get; set; }
            public Vector2[] ImmediateNeighborPositions { get; set; }
            public List<IEdge> Connections { get; set; }
            public object GameObject { get; set; }
            public Dictionary<object, float> DistanceFromStart { get; set; }

            public VertexData(Guid id, Vector2 position, Vector2[] possibleImmediateNeighborPositions)
            {
                ID = id;
                Position = position;
                ImmediateNeighborPositions = possibleImmediateNeighborPositions;
                DistanceFromStart = new Dictionary<object, float>();
                Connections = new List<IEdge>();
            }
        }
    }
}
