﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Graphing.POCO
{
    public partial class Graph
    {
        private class FindVertex
        {
            private Vector2 position;
            private List<IVertex> vertices;

            public FindVertex(Vector2 position, List<IVertex> vertices)
            {
                this.position = position;
                this.vertices = vertices;
            }

            public IVertex Find()
            {
                return vertices.Find(VertexMatch);
            }

            private bool VertexMatch(IVertex vertex)
            {
                return vertex.Position == position;
            }
        }
    }
}
