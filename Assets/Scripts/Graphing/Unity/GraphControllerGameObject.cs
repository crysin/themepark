﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Graphing.POCO;

namespace Assets.Scripts.Graphing.Unity
{
    public class GraphControllerGameObject : MonoBehaviour
    {
        private Graph graph = null;
        public Graph Graph { get { if (graph == null) { graph = new Graph(); } return graph; } }
        public float offset = 1.0f;
        public float gridSize = 4.0f;

        public IVertex CreateVertex(Vector3 position)
        {
            VertexGameObject graphGO = null;
            IVertex vertex = Graph.CreateVertex(new Vector2(position.x, position.z));
            if (vertex.GameObject == null)
            {
                GameObject debugvertex = (GameObject)Resources.Load("VertexDebug");
                GameObject newVertexGO = Instantiate(debugvertex);
                graphGO = newVertexGO.GetComponent<VertexGameObject>();
                graphGO.Init(vertex);
                vertex.SetGameObject(newVertexGO);
            }
            else
            {
                graphGO = (vertex.GameObject as GameObject).GetComponent<VertexGameObject>();
            }
            return vertex;
        }

        public IVertex GetVertexAtPosition(Vector3 position)
        {
            return graph.GetVertexAtPosition(new Vector2(position.x, position.z));
        }

        public Vector3 GetNearestWholeVector(Vector3 rawPosition)
        {
            Vector3 samplePosition = rawPosition;
            samplePosition -= Vector3.one * offset;
            samplePosition /= gridSize;
            samplePosition = new Vector3(Mathf.Round(samplePosition.x), Mathf.Round(samplePosition.y), Mathf.Round(samplePosition.z));
            samplePosition *= gridSize;
            samplePosition += Vector3.one * offset;
            return samplePosition;
        }

        public IVertex GetNearestVertexToPosition(Vector3 position)
        {
            Vector2 xzPosition = new Vector2(position.x, position.z);
            return graph.GetNearestVertexToPosition(xzPosition);
        }

        public void Update()
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 mousePosition = Input.mousePosition;
                Ray ray = Camera.main.ScreenPointToRay(mousePosition);
                RaycastHit rayHit = new RaycastHit();
                if (Physics.Raycast(ray, out rayHit, 90000f))
                {
                    Vector3 nearestWholeVector = GetNearestWholeVector(rayHit.point);
                    CreateVertex(nearestWholeVector);
                }
            }
        }
    }
}
