﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Graphing.POCO;

namespace Assets.Scripts.Graphing.Unity
{
    public class VertexGameObject : MonoBehaviour
    {
        IVertex vertex = null;
        [SerializeField]
        private System.Guid vertexName;
        public System.Guid VertexName { get { return vertexName; } }
        [SerializeField]
        private Vector2 xzPosition;
        public Vector2 XZPosition { get { return xzPosition; } }
        [SerializeField]
        private Vector2[] xzPossibleNeighborPositions;
        [SerializeField]
        private System.Collections.Generic.List<VertexGameObject> neighbors = new System.Collections.Generic.List<VertexGameObject>();
        [SerializeField]
        private TextMesh neighborCounterMesh = null;
        private Coroutine waitForEndOfFrame = null;

        private TextMesh cachedNeighborCounterMesh { get { return neighborCounterMesh != null ? neighborCounterMesh : GetComponentInChildren<TextMesh>(); } }

        public void Init(IVertex vertex)
        {
            this.vertex = vertex;
            vertex.DataUpdate += UpdateInfo;
            vertex.DataUpdate();
        }

        [ContextMenu("Update")]
        public void UpdateInfo()
        {
            vertexName = vertex.ID;
            xzPosition = vertex.Position;
            xzPossibleNeighborPositions = vertex.ImmediateNeighborPositions;
            gameObject.transform.position = new Vector3(xzPosition.x, 0, xzPosition.y);
            gameObject.name = vertexName.ToString();
            if (waitForEndOfFrame == null)
                waitForEndOfFrame = StartCoroutine(WaitForEndOfFrame());
        }

        private IEnumerator WaitForEndOfFrame()
        {
            yield return new WaitForEndOfFrame();
            waitForEndOfFrame = null;
            cachedNeighborCounterMesh.text = vertex.Connections.Count.ToString();
        }

        void OnDestroy()
        {
            vertex.DataUpdate -= UpdateInfo;
        }
    }
}
