﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.AI.Unity;
using Assets.Scripts.Graphing.Unity;

namespace Assets.Scripts.AI.Unity
{
    public class AgentsControllerGameObject : MonoBehaviour
    {
        public PathfinderGameObject CreateAgent(GraphControllerGameObject graph, Vector3 startPosition)
        {
            GameObject agentGO = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            agentGO.transform.position = startPosition;
            PathfinderGameObject agent = agentGO.AddComponent<PathfinderGameObject>();
            agent.Init(graph);
            return agent;
        }
    }
}
