﻿using Assets.Scripts.Graphing.POCO;
using Assets.Scripts.Graphing.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.AI.Unity
{
    public class PathfinderGameObject : MonoBehaviour
    {
        private PathGenerator pathGenerator;
        private GraphControllerGameObject graph = null;
        private Queue<IVertex> path = new Queue<IVertex>();
        private GameObject currentGoal = null;
        private Vector3 cachedPosition = Vector3.zero;
        private float time = 0;
        public System.Action<PathfinderGameObject> GoalReached = null;
        public System.Action MovementComplete = null;
        private Vector3 endGoalPosition;
        public void Init(GraphControllerGameObject graph)
        {
            pathGenerator = new PathGenerator();
            this.graph = graph;
            StartCoroutine(DelayedUpdate());
        }

        public void SetGoal(Vector3 goal)
        {
            endGoalPosition = goal;
            Vector3 myPosition = graph.GetNearestWholeVector(transform.position);
            IVertex myVertex = graph.GetNearestVertexToPosition(myPosition);
            if(goal.y == 0)
            {
                goal = new Vector2(goal.x, goal.z);
            }
            pathGenerator.GetPath(graph.Graph, myVertex, goal, PathGenerated);
        }

        private void PathGenerated()
        {
            List<IVertex> pathList = new List<IVertex>(pathGenerator.Path);
            if (pathList == null)
                return;

            path = new Queue<IVertex>(pathList);
            currentGoal = null;
            cachedPosition = transform.position;
            time = 0;
            currentGoal = GetCurrentGoal();
        }

        private IEnumerator DelayedUpdate()
        {
            while (true)
            {
                if(currentGoal != null)
                {
                    Debug.DrawLine(transform.position, endGoalPosition, Color.black);
                }
                yield return null;
                if (Time.frameCount % 2 == 0)
                {
                    if (pathGenerator.Finished == false)
                    {
                        pathGenerator.Update();
                    }
                    else
                    {
                        NavigatePath();
                    }
                }
            }
        }

        public void NavigatePath()
        {
            if (path.Count == 0)
            {
                if (GoalReached != null && pathGenerator.Finished == true)
                {
                    GoalReached(this);
                }
                return;
            }

            transform.position = Vector3.Lerp(cachedPosition, currentGoal.transform.position, time);
            time += Time.deltaTime;
            if (time > 1)
            {
                time = 0;
                currentGoal = GetCurrentGoal();
                cachedPosition = transform.position;
            }
            if(MovementComplete != null)
            {
                MovementComplete();
            }
        }
        private GameObject GetCurrentGoal()
        {
            GameObject goal = null;
            if(path.Count != 0)
            {
                goal = (GameObject)path.Dequeue().GameObject;
            }
            return goal;
        }
    }
}
