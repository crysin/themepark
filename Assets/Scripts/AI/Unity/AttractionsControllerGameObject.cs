﻿using Assets.Scripts.Graphing.POCO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.AI.Unity
{
    public class AttractionsControllerGameObject : MonoBehaviour
    {
        private List<Attraction> attractions = new List<Attraction>();
        public ReadOnlyCollection<Attraction> Attractions { get { return attractions.AsReadOnly(); } }

        public void AddAttraction(Vector3 position)
        {
            Attraction attraction = new Attraction();
            GameObject attractionGo = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            attractionGo.GetComponent<Renderer>().material.color = Color.blue;
            attractionGo.name = "Attraction";
            attractionGo.transform.position = position;
            attraction.GameObject = attractionGo;
            attraction.GoalPosition = position;
            attractions.Add(attraction);
        }
    }

    public class Attraction
    {
        public GameObject GameObject;
        public Vector3 GoalPosition;
    }
}
