﻿using Assets.Scripts.AI.Unity;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Graphing.Unity;
using Assets.Scripts.Graphing.POCO;
using System.Collections.Generic;
using System;
using System.Collections;

namespace Assets.Scripts.AI.Unity
{
    public class ArtificialIntelligenceManagerGameObject : MonoBehaviour
    {
        [SerializeField]
        private GraphControllerGameObject graphController = null;
        public GraphControllerGameObject GraphController { get { return graphController; } }

        [SerializeField]
        private AgentsControllerGameObject agentsController = null;
        public AgentsControllerGameObject AgentsController { get { return agentsController; } }

        [SerializeField]
        private AttractionsControllerGameObject attractionsController = null;
        public AttractionsControllerGameObject AttractionsController { get { return attractionsController; } }

        Action moveAgent = null;
        private int agentsToMoveBeforeFrameWait = 5;
        public void Init()
        {
           //StartCoroutine(NextAI());
        }

        public void CreateAgent(Vector3 goalPosition)
        {
            CreateAgent(goalPosition, Vector3.zero);
        }

        public void CreateAgent(Vector3 goalPosition, Vector3 startPosition)
        {
            PathfinderGameObject agent = agentsController.CreateAgent(graphController, startPosition);
            agent.GoalReached += GoalReached;
            agent.MovementComplete += MovementComplete;
            agent.SetGoal(goalPosition);
            moveAgent += agent.NavigatePath;
        }

        private void MovementComplete()
        {
            //NextAI();
        }

        private void GoalReached(PathfinderGameObject agent)
        {
            int nextGoal = UnityEngine.Random.Range(0, attractionsController.Attractions.Count - 1);
            Attraction attraction = attractionsController.Attractions[nextGoal];
            agent.SetGoal(attraction.GoalPosition);
        }
    }
}
